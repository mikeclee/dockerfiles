Unix Development Dockerfile
===========================

Based on Ubuntu 14.04

### Tools Available

* curl
* git
* htop
* man
* make
* python
* g++
* php
* golang
