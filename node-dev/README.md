Node Dockerfile
===============

Based on Ubuntu 14.04
Dependent on the node/Dockerfile Image

Image for creating a base-line Node development environment

#### References

* [Official Git Repo](https://github.com/nodejs/node)
* [Official Image](https://hub.docker.com/_/node/)
* [CURL Options](https://curl.haxx.se/docs/manpage.html)
