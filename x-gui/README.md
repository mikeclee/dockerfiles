GUI with X11 Dockerfile
=======================

Based on Ubuntu 14.04 and Node 4.3.1

Image for creating a base-line Ionic development environment


RUN apt-get update && apt-get install -y \
  x11-apps

DISPLAY=192.168.99.1:0.0
192.168.99.1

### Xming Information

* [Xming Official Site](http://www.straightrunning.com/XmingNotes/)
* `Xming -query <IP address of remote host> -nodecoration -screen 0 @2 -clipboard -wgl`
* `Xming -query <IP address of remote host> -nodecoration -screen 0 @1 -screen 1 @2 +xinerama -clipboard`
* [X11 Official Doc](http://www.x.org/releases/X11R7.7/doc/man/man1/)
* `Xming.exe -multiwindow -clipboard`
* [Xming Files and Locations](http://www.straightrunning.com/xmingnotes/trouble.php#head-13)
* Windows profile location: %ALLUSERSPROFILE%

[Xming & Docker](https://technology.amis.nl/2015/03/15/docker-take-two-starting-from-windows-with-linux-vm-as-docker-host/)
1. Look in the log for the Xdmcp info: `XdmcpRegisterConnection: newAddress`
1. Set in the VM or Host (Docker daemon host) - `export DISPLAY=<Xdmcp ID>:0.0`

* [UAF University Xming Usage](https://www.uaf.edu/arsc/knowledge-base/using-xming-x-server-for-/index.xml)

### Basic Android SDK commands

* android list sdk
* android update sdk --no-ui --filter
* echo 'y' | android update ...
    This will Auto-magically accept the license [y]
    * next version of SDK should have --accept-license

#### References

* [Official Site](http://ionicframework.com/)
* [Android SDK CLI](http://tools.android.com/recent/updatingsdkfromcommand-line)
* [Official Git Repo](https://github.com/nodejs/node)
* [Official Image](https://hub.docker.com/_/node/)
* [CURL Options](https://curl.haxx.se/docs/manpage.html)

* [Installing Chocolatey](https://chocolatey.org/)
    `@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin`

android list sdk
1- Android SDK Platform-tools, revision 23.1
   2- Android SDK Build-tools, revision 23.0.2
    5- SDK Platform Android 5.1.1, API 22, revision 2
    48- Android Support Repository, revision 25




    "Config": {
        "Hostname": "dev",
        "Domainname": "",
        "User": "",
        "AttachStdin": false,
        "AttachStdout": true,
        "AttachStderr": true,
        "Tty": false,
        "OpenStdin": false,
        "StdinOnce": false,
        "Env": [
            "DISPLAY=192.168.99.1:0.0",
            "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
        ],
        "Cmd": [
            "--user-data-dir=/data"
        ],
        "Image": "jess/chrome",
        "Volumes": null,
        "WorkingDir": "",
        "Entrypoint": [
            "google-chrome"
        ],
        "OnBuild": null,
        "Labels": {},
        "S



        # docker run -it \
#	--net host \ # may as well YOLO
#	--cpuset-cpus 0 \ # control the cpu
#	--memory 512mb \ # max memory it can use
#	-v /tmp/.X11-unix:/tmp/.X11-unix \ # mount the X11 socket
#	-e DISPLAY=unix$DISPLAY \
#	-v $HOME/Downloads:/root/Downloads \
#	-v $HOME/.config/google-chrome/:/data \ # if you want to save state
#	--device /dev/snd \ # so we have sound
#	-v /dev/shm:/dev/shm \
#	--name chrome \
#	jess/chrome

docker run -it --net host --cpuset-cpus 0 --memory 512mb -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY --name chrome jess/chrome

docker run -v /tmp/.X11-unix -e DISPLAY=$DISPLAY jess/tor-browser
#	-v /dev/snd:/dev/snd \
#	-e DISPLAY=unix$DISPLAY \
#	jess/tor-browser

sudo chmod 4755 /opt/google/chrome/chrome-sandbox
* http://golangcloud.blogspot.com/2014/06/run-x11-application-inside-docker.html
....
