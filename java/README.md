Java Dockerfile
===============

Based on Ubuntu 14.04
Running Oracle Java 1.8.0_73

### Run Command

* `docker run --rm leemike/java <command>`
* `docker-compose run java`

#### References

* [Official Image](https://hub.docker.com/_/java/)
* [Oracle Installation](http://docs.oracle.com/javase/8/docs/technotes/guides/install/linux_jdk.html#BJFJJEFG)
* [Environment Variables](http://www.cyberciti.biz/faq/linux-unix-set-java_home-path-variable/)
* [CURL Options](https://curl.haxx.se/docs/manpage.html)
