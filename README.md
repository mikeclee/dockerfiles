Dockerfiles
===========

* [Dockerfile Reference](https://docs.docker.com/engine/reference/builder/)

* [Docker Compose Reference](https://docs.docker.com/compose/compose-file)

## Docker-Compose

* Installation - `gc-leemike-ml1:chrome leemike$ curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose`

### References

* [Mac Installation](https://docs.docker.com/engine/installation/mac/)
* [Insecure Registry](http://stackoverflow.com/questions/30654306/allow-insecure-registry-in-host-provisioned-with-docker-machine)
* [Lukas Pustina Docker Info](https://blog.codecentric.de/en/2014/02/docker-registry-run-private-docker-image-repository/)

### Usefuls

* Find the public IP - `curl http://icanhazip.com`

## Informational

* Redis = REmote DIctionary Service

## Docker CLI

* docker rmi (docker images -f "dangling=true" -q)
    Remove any dangling <none> images
* docker rm -v (docker ps -aq)
    Remove any stopped AND none associated containers 
* docker ps -f status=exited
    Filter the output for specific status state
