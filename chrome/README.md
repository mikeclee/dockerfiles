Chrome Dockerfile
=================

Based on Ubuntu 14.04

### Run Command

* `docker run --rm --memory 512mb --net host --security-opt seccomp:unconfined -e DISPLAY=<host_ip>:0 <image_name>`

#### References

* [Jess Docker Github](https://github.com/jfrazelle/dockerfiles/blob/master/chrome/stable/Dockerfile)
* [Jess Docker Hub](https://hub.docker.com/r/jess/chrome/~/dockerfile/)
* [Ubuntu Official Unofficial Repository List](https://wiki.debian.org/UnofficialRepositories)
* [CURL Options](https://curl.haxx.se/docs/manpage.html)
