Jenkins Dockerfile
==================

Based on Ubuntu 14.04

### Description

* latest, 1.642.1 (Dockerfile)

### Run Command

* `docker run -p 8080:8080 -p 50000:50000 jenkins`

#### References

* [Official Docker Hub](https://hub.docker.com/_/jenkins/)
* [CURL Options](https://curl.haxx.se/docs/manpage.html)

#### Official Jenkins Image

* https://hub.docker.com/\_/jenkins/
* Latest stable version 1.642.4
* `docker run -p 8080:8080 -p 50000:50000 -v /your/home:/var/jenkins_home jenkins`

