﻿
### Uninstall Docker-Machine

1. Removing a machine deletes its VM from VirtualBox and from the `~/.docker/machine/machines` directory.
1. Remove the Docker Quickstart Terminal and Kitematic from your `“Applications”` folder.
1. Remove the docker, docker-compose, and docker-machine commands from the /usr/local/bin folder.
`$ rm /usr/local/bin/docker`
1. Delete the `~/.docker` folder from your system.

### Running on Windows

* Win32 path should be starting with '/c/...'

### Squash Docker Image File

* [docker-squash project](https://github.com/jwilder/docker-squash)
