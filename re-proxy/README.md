Reverse Proxy Dockerfile
========================

### Apache

* [Official Image](https://hub.docker.com/_/httpd/)
* [DigitalOcean Installation](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-14-04)

Reference: 
* (https://www.nginx.com/resources/admin-guide/reverse-proxy/)
* [Official?! WIKI](https://www.nginx.com/resources/wiki/start/)
* [jResponse Dynamic Re-Proxy](https://jresponse.net/blog/using-nginx-as-a-dynamic-reverse-proxy-for-docker-containers/)
* [DigitalOcean Guide to Nginx Installation](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-14-04-lts)

Tests:
* `namei -om`

Nginx.conf configuration is in order of these locations:
/usr/local/nginx/conf, /etc/nginx, or /usr/local/etc/nginx

Updates:
* /etc/nginx/conf.d/** will be the location for specific configurations
