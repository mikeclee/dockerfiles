
## Basic Steps

1. `docker run -d -p 8091-8094:8091-8094 -p 11210:11210 couchbase/server`
1. visit `http://localhost:8091/index.html` for the UI

### Developer Flow

1. `docker run -it --rm -p 8091-8094:8091-8094 -p 11210:11210 couchbase/server bash`
1. in the container run `couchbase-server`
1. visit `http://localhost:8091/index.html` for the UI


### References

* [Unofficial Vendor Image](https://hub.docker.com/r/couchbase/)
