Apache Httpd Dockerfile
=======================

Based on Ubuntu 14.04

Building with Dockerfile::HTTPD_VERSION with version 2.4.18

Use the compose override to easily change the listen port, configurations and mapping.

* `ports` to map any EXPOSE and VM host
* `volumes` to map the *htdocs*, *certificates* and *configurations*
     * `/usr/local/apache2/htdocs` for basic *htdocs*
     * `/usr/local/apache2/conf/` for all *configuration* information
     * `/usr/local/apache2/conf/httpd.conf` for main Apache Httpd configuration
     * `/usr/local/apache2/conf/server.crt` and `/usr/local/apache2/conf/server.key` for default SSL *certificates*

### Run Command

* `docker run --rm -v <host>:<container> leemike/httpd`

### Reference

* [Official Httpd Docker](https://hub.docker.com/_/httpd/)
* [DigitalOcean Installation](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-14-04)
* [Nginx Admin Guide for Reverse Proxy](https://www.nginx.com/resources/admin-guide/reverse-proxy/)
* [Official Nginx WIKI](https://www.nginx.com/resources/wiki/start/)
* [jResponse Dynamic Re-Proxy](https://jresponse.net/blog/using-nginx-as-a-dynamic-reverse-proxy-for-docker-containers/)
* [DigitalOcean Guide to Nginx Installation](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-14-04-lts)

#### Notes

* `namei -om`
* Nginx.conf configuration is in order of these locations:
    /usr/local/nginx/conf, /etc/nginx, or /usr/local/etc/nginx
* /etc/nginx/conf.d/** will be the location for specific configurations
* on Windows system, the path is /<drive>/...<path>
