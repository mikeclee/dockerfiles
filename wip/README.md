
### Docker Machine - Drivers

* [Oracle VirtualBox Drive Info](https://docs.docker.com/machine/drivers/virtualbox/)

### MAC - XQuartz

* [Official Site](http://www.xquartz.org/)
* [FAQ / Info](https://github.com/XQuartz/xquartz.github.io/blob/master/FAQs.md)
* [Run GUI on Mac Docker](http://blog.bennycornelissen.nl/bwc-gui-apps-in-docker-on-osx/) (&#x2714;)

#### Mac - XQuartz Configs

* `defaults write org.macosforge.xquartz.X11 nolisten_tcp 0` - Listen for TCP connection
* `defaults write org.macosforge.xquartz.X11 app_to_run /usr/bin/true` - remove the default XTerm app
* `netstat -an | grep 6000 &> /dev/null || open -a XQuartz` - start the application
* `export DISPLAY=:0` - set the default display ENV
* `xhost +<IP>` to add Authorization for X11 server connection - this is weak simple Auth

#### Mac - Sample RUNs

* `docker run -it --name wip --memory 512mb --net host -e DISPLAY=<host_ip>:0 <image_name> bash`
* `docker run --rm --memory 512mb --net host --security-opt seccomp:unconfined -e DISPLAY=<host_ip>:0 jess/chrome`

#### Mac - default?!

* DISPLAY = `/private/tmp/com.apple.launchd.aUIOc6bvpk/org.macosforge.xquartz:0`
* netstat -rn
