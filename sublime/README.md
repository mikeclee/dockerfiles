Sublime Editor Dockerfile
=========================

Based on Ubuntu 14.04

Using Sublime 3.3103 - this is controlled in the Dockfile::SUB_DOWNLOAD

### Run Command

* `docker run --rm --memory 512mb --net host --security-opt seccomp:unconfined -e DISPLAY=<host_ip>:0 <image_name>`
* `docker run --rm --memory 512mb --net host --security-opt seccomp:unconfined -v <host_folder>:<container_folder> -e DISPLAY=<host_ip>:0 <image_name>`

### Docker Compose Setup

1. Update the `docker-compose.override.yml` ENVIRONMENT::DISPLAY value to be the correct X11 server information
1. Adjust the `docker-compose.override.yml` volumes if desired - default mounting is the `/tmp` directory
1. `docker-compose up`

#### References

* [Official Sublime](http://www.sublimetext.com/)
* [Sublime 3](http://www.sublimetext.com/3)
* [Sublime 3 Command Line](https://www.sublimetext.com/docs/3/osx_command_line.html)
* [CURL Options](https://curl.haxx.se/docs/manpage.html)
* [Official Docker Compose Reference](https://docs.docker.com/compose/compose-file/)
* [Official Dockerfile Reference](https://docs.docker.com/engine/reference/builder/)
